pipeline {
    agent any

    stages {
        stage('Create Build Environment') {
            steps {
                echo 'Provisioning Build Environment....'
                sh '''
cd environment/

cat << EOF > input.tfvars
client_ip = "$(curl -s http://whatismyip.akamai.com/)/32"
EOF

terraform init
terraform apply -auto-approve -var-file input.tfvars

output=$(terraform output -json)

security_group_id=$(echo $output | jq '.security_group_id.value')
subnet_id=$(echo $output | jq '.subnet_id.value')
vpc_id=$(echo $output | jq '.vpc_id.value')
version=$(uuid)

cd ..

rm -rf ansible/ # Clear existing ansible configuration
git clone https://bitbucket.org/evoio-scratch/configuration.git ansible

cat << EOF > variables.json
{
    "security_group_id": $security_group_id,
    "subnet_id": $subnet_id,
    "vpc_id": $vpc_id,
    "version": "$version"
}
EOF
                '''
            }
        }
        stage('Run Packer') {
            steps {
                echo 'Building image(s)....'
                sh 'packer build -var-file variables.json template.json'
            }
        }
        stage('Destroy Build Environment') {
            steps {
                echo 'De-provisioning Build Environment....'
                sh '''
cd environment/
terraform destroy -auto-approve -var-file input.tfvars
cd ..
                '''
            }
        }
        stage('Update Infrastructure') {
            steps {
                echo 'Updating Infrastructure....'
                sh '''
rm -rf infrastructure/
git clone https://bitbucket.org/evoio-scratch/infrastructure.git infrastructure

cd infrastructure
terraform init
terraform apply -auto-approve

cd ..
rm -rf infrastructure/
                '''
            }
        }
    }
}