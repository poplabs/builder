# Builder
Contains the Packer template definition and the Terraform template for the build environment. The build environment provides SSH access to the builder.
