#
# OUTPUTS
# =============================================================================
#

output "vpc_id" {
  description = "ID of the VPC to deploy the instance into"
  value       = "${aws_vpc.main.id}"
}

output "subnet_id" {
  description = "ID of the subnet to deploy the instance into"
  value       = "${aws_subnet.public.id}"
}

output "security_group_id" {
  description = "ID of the security group to use for the instance"
  value       = "${aws_security_group.builder.id}"
}
