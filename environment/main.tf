terraform {
  backend "s3" {
    bucket = "demo-state"
    key    = "pipeline-demo/builder/state/terraform.tfstate"
    region = "eu-west-2"
  }
}

provider "aws" {
  region = "eu-west-2"
}

resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_subnet" "public" {
  cidr_block        = "10.0.1.0/24"
  availability_zone = "eu-west-2a"
  vpc_id            = "${aws_vpc.main.id}"
}

resource "aws_route_table" "public" {
  vpc_id = "${aws_vpc.main.id}"
}

resource "aws_internet_gateway" "main" {
  vpc_id = "${aws_vpc.main.id}"
}

resource "aws_route" "internet" {
  route_table_id         = "${aws_route_table.public.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.main.id}"
}

resource "aws_route_table_association" "public" {
  route_table_id = "${aws_route_table.public.id}"
  subnet_id      = "${aws_subnet.public.id}"
}

resource "aws_security_group" "builder" {
  name        = "Builder Security Group"
  description = "Security group for the builder instance(s)"
  vpc_id      = "${aws_vpc.main.id}"
}

resource "aws_security_group_rule" "builder_ingress_22" {
  type        = "ingress"
  from_port   = 22
  to_port     = 22
  protocol    = "tcp"
  cidr_blocks = ["${var.client_ip}"]

  security_group_id = "${aws_security_group.builder.id}"
}

resource "aws_security_group_rule" "builder_egress_all" {
  type        = "egress"
  from_port   = 0
  to_port     = 65535
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = "${aws_security_group.builder.id}"
}
