# Builder / Environment
Provisions the environment required to host the builder instance - mostly required to give SSH access to the instance running Packer.